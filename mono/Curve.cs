using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Permissions;

public class Curve
{
    private readonly List<Lane> lanes;
    private readonly Dictionary<int, Piece> piecesInCurve;
    private double maxAngle;
    public Dictionary<int, double> laneLengths;
    private double slipForce;
    public double angle { get; set; }
    public double radius { get; set; }
    private double?[] maxSpeed = new double?[12];

    public double SlipForce
    {
        get { return slipForce; }
        set { slipForce = Math.Min(1, value > 0 ? value : .1); }
    }

    public Curve(List<Lane> lanes)
    {
        this.lanes = lanes;
        laneLengths = new Dictionary<int, double>();
        piecesInCurve = new Dictionary<int, Piece>();
    }

    internal void AddPiece(Piece piece, int pieceIndex)
    {
        if (!Matches(piece))
            throw new Exception("Illegal piece used in curve.");

        foreach (var lane in lanes)
        {
            if (!laneLengths.ContainsKey(lane.index))
            {
                laneLengths[lane.index] = 0;
            }
            laneLengths[lane.index] += piece.GetLength(lane.distanceFromCenter);
        }

        piecesInCurve.Add(pieceIndex, piece);
        radius = piece.radius.Value;
        angle += piece.angle.Value;

        // Magic numbers are awesome.
        SlipForce = radius/250;
    }

    public int FirstPiece
    {
        get { return piecesInCurve.Keys.Min(); }
    }

    public double MaxSpeedForLane(int laneIndex){
        return maxSpeed[laneIndex].HasValue ? maxSpeed[laneIndex].Value : Math.Sqrt(SlipForce*GetLaneRadius(laneIndex));
    }

    public void SetMaxSpeedForLane(int laneIndex, double speed){
        maxSpeed[laneIndex] = speed;
    }

    public double GetLaneRadius(int laneIndex)
    {
        var distanceFromCenter = lanes[laneIndex].distanceFromCenter;
        if (angle > 0)
            distanceFromCenter = -distanceFromCenter;
        return radius + distanceFromCenter;
    }

    public double GetCentripetalForceForLaneAtSpeed(double speed, int laneIndex)
    {
        return Math.Pow(speed, 2)/GetLaneRadius(laneIndex);
    }

    public bool ContainsPiece(int pieceIndex)
    {
        return piecesInCurve.ContainsKey(pieceIndex);
    }

    public double PercentageThroughCurve(int laneIndex, int pieceIndex, double inPieceDistance)
    {
        var travelledThroughCurve = 0d;
        var minIndex = piecesInCurve.Keys.Min();
        for (int i = minIndex; i < pieceIndex; i++)
        {
            travelledThroughCurve += piecesInCurve[i].GetLength(lanes[laneIndex].distanceFromCenter);
        }

        travelledThroughCurve += inPieceDistance;
        return travelledThroughCurve/laneLengths[laneIndex];
    }

    public bool Matches(Piece piece)
    {
        var matchesRadius = piece.radius.HasValue && radius == 0 || Math.Abs(piece.radius.Value - radius) == 0;
        var matchesDirection = piece.angle.HasValue && angle == 0 || Math.Sign(angle) == Math.Sign(piece.angle.Value);

        return matchesRadius && matchesDirection;
    }

    public void SetCurrentAngle(double currentAngle)
    {
        var absCurrentAngle = Math.Abs(currentAngle);
        if (maxAngle < absCurrentAngle)
        {
            maxAngle = absCurrentAngle;
        }
    }

    public bool HasSwitch()
    {
        return piecesInCurve.Any(p => p.Value.@switch);
    }
}
