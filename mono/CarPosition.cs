﻿using System;

public class CarPosition : IComparable<CarPosition>
{
    public CarId id { get; set; }
    public double angle { get; set; }
    public PiecePosition piecePosition { get; set; }

    public int CompareTo(CarPosition obj)
    {
        return piecePosition.CompareTo(obj.piecePosition);
    }
}

public class PiecePosition : IComparable<PiecePosition>
{
    public int pieceIndex { get; set; }
    public double inPieceDistance { get; set; }
    public CarLane lane { get; set; }
    public int lap { get; set; }

    public int CompareTo(PiecePosition obj)
    {
       // if (lap.CompareTo(obj.lap) != 0) return lap.CompareTo(obj.lap);
        return pieceIndex.CompareTo(obj.lap) != 0 ? pieceIndex.CompareTo(obj.pieceIndex) : inPieceDistance.CompareTo(obj.inPieceDistance);
    }
}

public class CarLane
{
    public int startLaneIndex { get; set; }
    public int endLaneIndex { get; set; }
}