﻿using System;
using System.Collections.Generic;
using System.Linq;

public class RaceWrapper
{
    public Race race { get; set; }
}

public class Race
{
    public Track Track { get; set; }
    public List<Car> cars { get; set; }
    public RaceSession raceSession { get; set; }
    public LinkedList<Piece> Switches { get; private set; }

    public int NextPieceAfterPosition(int position)
    {
        return position + 1 >= Track.pieces.Count ? 0 : position + 1;
    }

    private int PieceBeforePosition(int position)
    {
        return position - 1 < 0 ? Track.pieces.Count - 1 : position - 1;
    }

    public void MapSwitches()
    {
        Switches = new LinkedList<Piece>();

        var currentSwitch = Track.pieces.First(p => p.@switch);
        var firstSwitch = currentSwitch;
        var length = 0;
        // Start looping starting at the last track piece.
        for (int i = NextPieceAfterPosition(Track.pieces.IndexOf(currentSwitch)); ; i = NextPieceAfterPosition(i))
        {
            var piece = Track.pieces[i];
            piece.index = i;
            if (piece.@switch)
            {
                if (length > 0)
                    currentSwitch.insideTrack = InsideTrack.Right;
                else if (length < 0)
                    currentSwitch.insideTrack = InsideTrack.Left;
                else if (length == 0)
                    currentSwitch.insideTrack = InsideTrack.None;

                Switches.AddLast(currentSwitch);

                currentSwitch = piece;
                length = 0;
            }

            if (piece.radius.HasValue && !piece.@switch)
            {
                if (piece.angle > 0)
                    length += (int)piece.length;
                if (piece.angle < 0)
                    length -= (int)piece.length;
            }

            if (i == Track.pieces.IndexOf(firstSwitch)) break;
        }
    }

    public void MapCurves()
    {
        Curves = new List<Curve>();
        Curve currentCurve = null;
        for (int i = 0; i < Track.pieces.Count; i++)
        {
            var piece = Track.pieces[i];
            if (piece.radius.HasValue)
            {
                if (currentCurve != null && !currentCurve.Matches(piece))
                {
                    Curves.Add(currentCurve);
                    currentCurve = null;
                }

                currentCurve = currentCurve ?? new Curve(Track.lanes);
                currentCurve.AddPiece(piece, i);
            }
            else if (currentCurve != null)
            {
                Curves.Add(currentCurve);
                currentCurve = null;
            }
        }
        if (currentCurve != null)
        {
            Curves.Add(currentCurve);
        }
    }

    public void MapBoostZones()
    {
        var firstCurve = Track.pieces.First(p => p.radius.HasValue);
        int firstPieceAfterCurve = -1;
        var lengthToNextBigCurve = 0d;
        for (int i = NextPieceAfterPosition(Track.pieces.IndexOf(firstCurve)); ; i = NextPieceAfterPosition(i))
        {
            var piece = Track.pieces[i];
            if (!piece.radius.HasValue)
            {
                lengthToNextBigCurve += piece.length.Value;
            }
            else if (piece.radius > 200 || Math.Abs(Curves.Single(c => c.ContainsPiece(i)).angle) < 30)
            {
                lengthToNextBigCurve += piece.length.Value;
            }
            else
            {
                if (firstPieceAfterCurve != -1)
                {
                    Track.pieces[firstPieceAfterCurve].DistanceToNextBigCurve = lengthToNextBigCurve;
                    lengthToNextBigCurve = 0;
                }

                while (Track.pieces[i].radius.HasValue)
                {
                    i = NextPieceAfterPosition(i);
                }
                firstPieceAfterCurve = i--;
            }

            if (piece == firstCurve)
            {
                break;
            }
        }
    }

    public List<Curve> Curves { get; set; }

    public int FindPreviousCurve(Car car)
    {
        int previousCurve = car.Position.piecePosition.pieceIndex;
        var outOfCurve = !car.IsInCurve();
        Curve currentCurve = null;
        if (car.IsInCurve())
        {
            return previousCurve;
        }
        while (outOfCurve)
        {
            previousCurve = PieceBeforePosition(previousCurve);
            if (Track.pieces[previousCurve].radius.HasValue)
            {
                outOfCurve = false;
            }
        }

        return previousCurve;
    }

    public int FindNextCurve(Car car)
    {
        int nextCurveIndex = car.Position.piecePosition.pieceIndex;
        var outOfCurve = !car.IsInCurve();
        Curve currentCurve = null;
        if (car.IsInCurve())
        {
            currentCurve = Curves.Single(c => c.ContainsPiece(car.CurrentPieceIndex));
        }
        while (!outOfCurve)
        {
            nextCurveIndex = NextPieceAfterPosition(nextCurveIndex);
            if (!currentCurve.ContainsPiece(nextCurveIndex))
            {
                outOfCurve = true;
            }
        }
        while (true)
        {
            if (Track.pieces[nextCurveIndex].radius.HasValue)
            {
                break;
            }
            nextCurveIndex = NextPieceAfterPosition(nextCurveIndex);
        }

        return nextCurveIndex;
    }

    public double CalculateDistanceToPiece(CarPosition position, int piece)
    {
        double distanceToPiece = 0;
        for (int i = position.piecePosition.pieceIndex; ; i = NextPieceAfterPosition(i))
        {
            if (i == piece)
                break;
            if (i == position.piecePosition.pieceIndex)
                distanceToPiece += Track.pieces[i].GetLength(Track.lanes[position.piecePosition.lane.endLaneIndex].distanceFromCenter) - position.piecePosition.inPieceDistance;
            else
                distanceToPiece += Track.pieces[i].GetLength(Track.lanes[position.piecePosition.lane.endLaneIndex].distanceFromCenter);
        }
        return distanceToPiece;
    }

    public Piece GetPiece(int pieceIndex)
    {
        return Track.pieces[pieceIndex];
    }

    public Lane GetLane(int laneIndex)
    {
        return Track.lanes[laneIndex];
    }

    public Piece FindNextSwitch(int index)
    {
        for (int i = NextPieceAfterPosition(index);; i = NextPieceAfterPosition(i))
        {
            if (Track.pieces[i].@switch)
            {
                return Track.pieces[i];
            }
        }
    }
}

public class Track
{
    public string id { get; set; }
    public string name { get; set; }
    public List<Piece> pieces { get; set; } 
    public List<Lane> lanes { get; set; }
}

public class RaceSession
{
    public int laps { get; set; }
    public int maxLapTimeMs { get; set; }
    public bool quickRace { get; set; }
}

public class CarId
{
    public string name { get; set; }
    public string color { get; set; }
}

public class CarDimension
{
    public double length { get; set; }
    public double width { get; set; }
    public double guideFlagPosition { get; set; }
}

public class Lane
{
    public int distanceFromCenter { get; set; }
    public int index { get; set; }

    public Direction OffsetDirection
    {
        get { return distanceFromCenter < 0 ? Direction.Left : distanceFromCenter == 0 ? Direction.None : Direction.Right; }
    }
}

public class Piece
{
    public double? length { get; set; }
    public bool @switch { get; set; }
    public int? radius { get; set; }
    public double? angle { get; set; }
    public InsideTrack insideTrack { get; set; }
    public bool SwitchedLanes { get; set; }
    public int? index { get; set; }

    public Direction Direction
    {
        get
        {
            if (angle.HasValue)
            {
                return angle.Value < 0 ? Direction.Left : Direction.Right;
            }

            return Direction.None;
        }
    }

    public double DistanceToNextBigCurve { get; set; }

    public double GetLength(double distanceFromCenter)
    {
        double modifiedLength = 0;
        if (angle.HasValue)
        {
            // Right turn, we need to adjust the radius the opposite direction
            if (angle.Value > 0)
                distanceFromCenter = -distanceFromCenter;

            modifiedLength = Math.PI * (radius.Value + distanceFromCenter) * (Math.Abs(angle.Value) / 180);
        }
        else
        {
            modifiedLength = length.Value;
        }

        return modifiedLength + (SwitchedLanes ? 2.06 : 0);
    }
}

public enum InsideTrack
{
    Left,
    Right,
    None
}

public enum Direction
{
    Left,
    Right,
    None,
    Both
}