using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using Newtonsoft.Json;
using System.Threading;

public class Bot{
    public static void Main(string[] args) {
        string host = args[0];
        int port = int.Parse(args[1]);
        string botName = args[2];
        string botKey = args[3];
        int racers = 2;

        Console.WriteLine("Connecting to {0}:{1} as {2}/{3}", host, port, botName, botKey);


        using (TcpClient client = new TcpClient(host, port))
        {
            NetworkStream stream = client.GetStream();
            StreamReader reader = new StreamReader(stream);
            StreamWriter writer = new StreamWriter(stream);
            writer.AutoFlush = true;


#if !DEBUG
            new Bot(reader, writer, new Join(botName, botKey));
#else
            //new Bot(reader, writer, new CreateRace(botName, botKey, racers, "imola"));
            new Bot(reader, writer, new JoinRace(botName, botKey, racers));
#endif
            Console.ReadLine();
        }

        
        
        
        //for (int i = 0; i < racers; i++)
        //{
        //    TcpClient client = new TcpClient(host, port);


        //    NetworkStream stream = client.GetStream();
        //    StreamReader reader = new StreamReader(stream);
        //    StreamWriter writer = new StreamWriter(stream);
        //    writer.AutoFlush = true;
        //    int x = i;
        //    var workerThread = new Thread(() => newBot(x, reader, writer, botKey, racers));
        //    workerThread.IsBackground = true;
        //    workerThread.Start();
        //    Thread.Sleep(2000);


        //    Console.ReadLine();
        //}
        
    }
    public static void newBot(int i, StreamReader reader, StreamWriter writer, string botKey, int racers)
    {
        if (i == 0)
        {
            new Bot(reader, writer, new CreateRace("car " + i, botKey, racers, "germany"));

        }
        new Bot(reader, writer, new JoinRace("car " + i, botKey, racers));

    }

    private StreamWriter writer;
    private Race race;
    private List<CarId> crashedCars = new List<CarId>();
#if DEBUG
    private TextWriter log = new StreamWriter(@"C:\Temp\botLog.txt");
#endif
    
    Bot(StreamReader reader, StreamWriter writer, SendMsg join) {
        this.writer = writer;
        string line;

        send(join);

        while((line = reader.ReadLine()) != null)
        {
            Stopwatch sw = Stopwatch.StartNew();
            MsgWrapper msg = JsonConvert.DeserializeObject<MsgWrapper>(line);
            switch(msg.msgType) {
                case "carPositions":
                    send(MakeDecision(msg.data));
                    break;
                case "join":
                    ourCar.Status = "Joined";
                    //Console.WriteLine("Joined");
                    send(new Ping());
                    break;
                case "crash":
                    var carCrash = JsonConvert.DeserializeObject<CarId>(msg.data.ToString());
                    if (carCrash.color == ourCar.id.color)
                    {
                        ourCar.ProcessCrash();
                    }
                    else
                    {
                        crashedCars.Add(carCrash);
                    }
                    ourCar.Status = string.Format("Car {0} crashed", carCrash.name);
                    //Console.WriteLine("Car {0} crashed", carCrash.name);
                    break;
                case "spawn":
                    var carSpawn = JsonConvert.DeserializeObject<CarId>(msg.data.ToString());
                    if (carSpawn.color == ourCar.id.color)
                    {
                        ourCar.ProcessSpawn();
                        ourCar.Status = "Spawned";
                    }
                    else
                    {
                        crashedCars.RemoveAll(c => c.color == carSpawn.color);
                    }
                    //Console.WriteLine("Car {0} spawned", carSpawn);
                    break;
                case "createRace":
                    ourCar.Status = "Created new race";
                    //Console.WriteLine("Created new race");
                    send(new Ping());
                    break;
                case "joinRace":
                    Console.WriteLine("Joined existing race");
                    send(new Ping());
                    break;
                case "gameInit":
                    Console.WriteLine("Race init");
                    InitRace(msg.data);
                    send(new Ping());
                    break;
                case "gameEnd":
                    Console.WriteLine("Race ended");
                    send(new Ping());
                    break;
                case "gameStart":
                    Console.WriteLine("On your marks, get set, GO!");
                    Console.WriteLine(" Throttle | Speed | Max Speed | Angle | Piece | Turbo | Lap | Status");
                    send(new Ping());
                    break;
                case "yourCar":
                    ourCar.Status = "Car set";
                    Console.WriteLine("Car set");
                    send(SetCar(msg.data));
                    break;
                case "turboAvailable":
                    //Console.WriteLine("Turbo Available.");
                    SetTurbo(msg.data);
                    send(new Ping());
                    break;
                case "turboStart":
                    //Console.WriteLine("Turbo Started.");
                    SetTurboOn(msg.data);
                    send(new Ping());
                    break;
                case "turboEnd":
                    //Console.WriteLine("Turbo Ended.");
                    SetTurboOff(msg.data);
                    send(new Ping());
                    break;
                case "lapFinished":
                    SetLapData(msg.data);
                    send(new Ping());
                    break;
                default:
                    Console.WriteLine("Unknown response for message type {0}: {1}", msg.msgType, msg.data);
                    send(new Ping());
                    break;
            }
            //Console.WriteLine(sw.ElapsedMilliseconds);
        }
#if DEBUG
        log.Close();
#endif
    }

    private void SetLapData(object data){
        ourCar.Lap = JsonConvert.DeserializeObject<LapFinished>(data.ToString());
    }

    private void SetTurboOff(object data)
    {
        var car = JsonConvert.DeserializeObject<CarId>(data.ToString());
        if (ourCar.id.color == car.color)
        {
            ourCar.TurboOn = false;
        }
    }

    private void SetTurboOn(object data)
    {
        var car = JsonConvert.DeserializeObject<CarId>(data.ToString());
        if (ourCar.id.color == car.color)
        {
            ourCar.TurboOn = true;
        }
    }

    private void SetTurbo(object data)
    {
        ourCar.SetTurbo(JsonConvert.DeserializeObject<TurboAvailable>(data.ToString()));
    }

    private SendMsg SetCar(object data){
        ourCar.id = JsonConvert.DeserializeObject<CarId>(data.ToString());
        ourCar.Race = race;
        ourCar.Lap = new LapFinished{LapTime = new lapTime{lap = -1}};
        return new Ping();
    }

    private Car ourCar = new Car();

    private SendMsg MakeDecision(object data)
    {
        //if (ourCar.id.name == "car 1")
        //    return new Throttle(.3);
        var carPositions = JsonConvert.DeserializeObject<List<CarPosition>>(data.ToString());
        ourCar.SetPosition(carPositions);

        var curve = race.Curves.SingleOrDefault(c => c.ContainsPiece(ourCar.CurrentPieceIndex));
        if (curve != null)
        {
            curve.PercentageThroughCurve(ourCar.Position.piecePosition.lane.endLaneIndex,
                ourCar.CurrentPieceIndex, ourCar.Position.piecePosition.inPieceDistance);
        }

        // TODO: Check next 3 curves to determine if we're going too fast.
        var nextCurveIndex = race.FindNextCurve(ourCar);
        double distanceToCurve = race.CalculateDistanceToPiece(ourCar.Position, nextCurveIndex);

        var nextCurve = race.Curves.Single(c => c.ContainsPiece(nextCurveIndex));
        var laneIndex = ourCar.ExpectedLaneAt(nextCurveIndex);
        var maxSpeed = nextCurve.MaxSpeedForLane(laneIndex);
        var distanceToSafeSpeed = ourCar.CalculateDistanceToSafeSpeed(maxSpeed);

        // Check curves further ahead to see if we need to slow down faster.
        var speedDifference = distanceToCurve - distanceToSafeSpeed;
        var checkCurve = nextCurve;
        for (int i = 1; i < 4; i++)
        {
            var curveCheckIndex = race.Curves.IndexOf(checkCurve) + 1;
            if (curveCheckIndex == race.Curves.Count)
                curveCheckIndex = 0;
            checkCurve = race.Curves[curveCheckIndex];

            var maxSpeedForLane = checkCurve.MaxSpeedForLane(laneIndex);
            var distanceToCheckCurve = race.CalculateDistanceToPiece(ourCar.Position, checkCurve.FirstPiece);
            var distanceToSafeSpeedForCheckCurve = ourCar.CalculateDistanceToSafeSpeed(maxSpeedForLane);
            if (speedDifference > distanceToCheckCurve - distanceToSafeSpeedForCheckCurve)
            {
                if (maxSpeedForLane < maxSpeed)
                {
                    maxSpeed = maxSpeedForLane;
                    distanceToCurve = distanceToCheckCurve;
                    distanceToSafeSpeed = distanceToSafeSpeedForCheckCurve;
                }
            }
        }
        
        if (ourCar.ShouldSwitch(carPositions, crashedCars))
        {
            return ourCar.MakeSwitch();
        }

        string reason;
        if (ourCar.TurboAvailable != null && !ourCar.IsInCurve() && ourCar.CurrentPiece.DistanceToNextBigCurve > 200) //== race.Track.pieces.Max(p => p.DistanceToNextBigCurve))
        {
            ourCar.TurboAvailable = null;
            //Console.WriteLine("Turbo!!!!!!");
            return new Turbo();
        }

        if (ourCar.Speed == 0)
        {
            reason = "Just started or crashed, max throttle.";
            ourCar.Throttle = 1;
        }
        else if (race.Track.pieces.IndexOf(race.GetPiece(nextCurveIndex)) < ourCar.Position.piecePosition.pieceIndex && 
            !ourCar.IsInCurve() &&
            race.raceSession.laps > 0 && 
            ourCar.Position.piecePosition.lap == race.raceSession.laps - 1)
        {
            reason = "Last lap, no turns left.";
            ourCar.Throttle = 1;
        }
        else if (Math.Abs(ourCar.Angle) > 45 && distanceToCurve < ourCar.Speed * 10 || ourCar.AngleAcceleration < -2 && Math.Sign(ourCar.Angle) != Math.Sign(curve.angle))
        {
            reason = "Too sharp into curve.";
            ourCar.Throttle = 0;
        }
        else if (distanceToCurve < distanceToSafeSpeed + ourCar.Speed && !(ourCar.IsInCurve() && ourCar.WillHitMaxAngle()))
        {
            if (ourCar.Speed < maxSpeed && maxSpeed - ourCar.Speed < .1)
            {
                reason = "At safe speed.";
                ourCar.Throttle = maxSpeed;
            }
            else if (ourCar.Speed < maxSpeed)
            {
                reason = "Lower than max speed.";
                ourCar.Throttle += .1;
            }
            else
            {
                reason = "Slowing down for curve.";
                ourCar.Throttle = 0;
            }
        }
        else if (ourCar.IsInCurve())
        {
            if (ourCar.AngleAccelerationTooHigh)
            {
                reason = "Twisting too fast.";
                ourCar.Throttle = 0;
            }
            if (ourCar.WillHitMaxAngle())
            {
                reason = "Will hit max angle.";
                var brake = 1 - ourCar.Angle/59;
                ourCar.Throttle -= brake >= 0 ? brake : 0;
            }
            else
            {
                reason = "Will not hit max angle.";
                ourCar.Throttle += .1;
            }
        }
        else
        {
            reason = "Default";
            ourCar.Throttle = 1;
        }

        Console.SetCursorPosition(0, Console.CursorTop);
        Console.Write("\r    {0,-6:N1}|  {1,-5:N2}|    {2,-7:N2}|  {3,-5:####}|  {4,-5}|  {5,-5}| {6,-4}|  {7,-40}", 
            ourCar.Throttle, ourCar.Speed, maxSpeed, ourCar.Angle, ourCar.CurrentPieceIndex, ourCar.TurboOn ? "USE" : ourCar.TurboAvailable != null ? "GOT" : "",
            ourCar.Lap.LapTime.lap + 2, ourCar.Status);
        //Console.WriteLine("Max Speed: {0}, Speed: {1}", maxSpeed, ourCar.Speed);
        //Console.WriteLine("{4}, {0}, {1}, {2}, {3}, {5}", ourCar.Speed, ourCar.Angle, ourCar.AngleSpeed,
            //curve == null ? 0 : curve.radius + race.Track.lanes[ourCar.Position.piecePosition.lane.endLaneIndex].distanceFromCenter, ourCar.Throttle, reason);
#if DEBUG
        log.WriteLine("{4}, {0}, {1}, {2}, {3}, {5}", ourCar.Speed, ourCar.Angle, ourCar.AngleSpeed,
            curve == null ? 0 : curve.radius + race.Track.lanes[ourCar.Position.piecePosition.lane.endLaneIndex].distanceFromCenter, ourCar.Throttle, reason);
        log.Flush();
#endif
        return new Throttle(ourCar.Throttle);
    }

    private void InitRace(object data)
    {
        if (race == null)
        {
            race = JsonConvert.DeserializeObject<RaceWrapper>(data.ToString()).race;
            ourCar.dimensions = race.cars.Single(c => c.id.color == ourCar.id.color).dimensions;
            ourCar.Race = race;
            CalculateMissingValues();
            MapTrack();
        }
        ourCar.Init();
    }

    private void CalculateMissingValues()
    {
        foreach (var piece in race.Track.pieces)
        {
            if (piece.radius.HasValue)
                piece.length = (Math.Abs(piece.angle.Value)*(Math.PI/180))*piece.radius;
        }
    }

    private void MapTrack()
    {
        race.MapSwitches();
        race.MapCurves();
        race.MapBoostZones();
    }



    private void send(SendMsg msg)
    {
        var json = msg.ToJson();
        //Console.WriteLine(json);
        writer.WriteLine(json);
    }
}

public class LapFinished : SendMsg
{
    public car Car { get; set; }
    public lapTime LapTime { get; set; }
    public raceTime RaceTime { get; set; }
    public ranking Ranking { get; set; }

    protected override string MsgType(){
        return "lapFinished";
    }

    protected override object MsgData(){
        return "ping";
    }
}

public class car
{
    public string name { get; set; }
    public string color { get; set; }
}

public class lapTime
{
    public int lap { get; set; }
    public int ticks { get; set; }
    public int millis { get; set; }
}

public class raceTime
{
    public int laps { get; set; }
    public int ticks { get; set; }
    public int millis { get; set; }
}

public class ranking
{
    public int overall { get; set; }
    public int fastestLap { get; set; }
}

public class Turbo : SendMsg
{
    protected override string MsgType()
    {
        return "turbo";
    }

    protected override object MsgData()
    {
        return "Vrrroooooooooooooooom!!!!!!!";
    }
}

public class TurboAvailable
{
    public double turboDurationMilliseconds { get; set; }
    public double turboDurationTicks { get; set; }
    public double TurboFactor { get; set; }
}

class MsgWrapper {
    public string msgType;
    public Object data;

    public MsgWrapper(string msgType, Object data) {
        this.msgType = msgType;
        this.data = data;
    }
}

public abstract class SendMsg {
    public string ToJson() {
        return JsonConvert.SerializeObject(new MsgWrapper(this.MsgType(), this.MsgData()));
    }
    protected virtual Object MsgData() {
        return this;
    }

    protected abstract string MsgType();
}

class Join: SendMsg {
    public string name;
    public string key;

    public Join(string name, string key) {
        this.name = name;
        this.key = key;
    }

    protected override string MsgType() { 
        return "join";
    }
}

class CreateRace : SendMsg{
    public BotId botId ;
    public string password;
    public int carCount;
    public string trackName;

    public CreateRace(string name, string key, int racers, string trackName){
        this.botId = new BotId {
            key = key,
            name = name,
        };
        this.trackName = trackName;
        password = "pencil";
        carCount = racers;
    }

    protected override string MsgType(){
        return "createRace";
    }
}

class JoinRace : SendMsg {
    public BotId botId;
    public string password;
    public int carCount;
    public string trackName;

    public JoinRace(string name, string key, int racers) {
        this.botId = new BotId {
            key = key,
            name = name,
        };
        trackName = "imola";
        password = "pencil";
        carCount = racers;
    }

    protected override string MsgType() {
        return "joinRace";
    }
}

internal class BotId{
    public string name;
    public string key;
}

class Ping: SendMsg {
    protected override string MsgType() {
        return "ping";
    }
}

class Throttle: SendMsg {
    public double value;

    public Throttle(double value) {
        this.value = value;
    }

    protected override Object MsgData() {
        return this.value;
    }

    protected override string MsgType() {
        return "throttle";
    }
}

public class Switch : SendMsg
{
    private string direction;

    public Switch(string direction)
    {
        this.direction = direction;
    }

    protected override object MsgData()
    {
        return direction;
    }

    protected override string MsgType()
    {
        return "switchLane";
    }
}


