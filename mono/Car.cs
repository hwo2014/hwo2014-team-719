﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Security;
using System.Runtime.InteropServices;

public class Car
{
    private CarPosition position;
    private CarPosition previousPosition;
    private double previousAngle;
    private double angle;
    private LinkedListNode<Piece> switchList;
    private bool pendingSwitch;
    private Direction pendingDirection;
    private double throttle;
    private double previousAngleSpeed;
    private double previousSpeed;
    private CarPosition carInFrontOfMe;
    private int distanceInPieces;
    public LapFinished Lap;

    public string Status { get; set; }
    public CarId id { get; set; }
    public CarDimension dimensions { get; set; }
    public Race Race { get; set; }
    public Piece CurrentPiece { get; private set; }
    public bool AngleAccelerationTooHigh {
        get { return timeangleAccelerationAbove4 > 2; }
    }

    public double Acceleration
    {
        get { return Speed - previousSpeed; } 
    }

    public double Throttle
    {
        get { return throttle; }
        set { throttle = Math.Max(0, Math.Min(1, value)); }
    }

    public int CurrentPieceIndex
    {
        get { return Position.piecePosition.pieceIndex; }
    }

    public bool Crashed { get; set; }

    public double Speed
    {
        get
        {
            if (previousPosition == null)
            {
                return 0;
            }
            if (previousPosition.piecePosition.pieceIndex == CurrentPieceIndex)
            {
                return position.piecePosition.inPieceDistance - previousPosition.piecePosition.inPieceDistance;
            }
            var distanceFromCenter = Race.Track.lanes[previousPosition.piecePosition.lane.endLaneIndex].distanceFromCenter;

            var distanceTraveled = Race.Track.pieces[previousPosition.piecePosition.pieceIndex].GetLength(distanceFromCenter) - previousPosition.piecePosition.inPieceDistance;
            var trackIndex = Race.NextPieceAfterPosition(previousPosition.piecePosition.pieceIndex);
            while (trackIndex != CurrentPieceIndex)
            {
                distanceTraveled += Race.Track.pieces[trackIndex].GetLength(distanceFromCenter);
                trackIndex = Race.NextPieceAfterPosition(trackIndex);
            }
            distanceTraveled += position.piecePosition.inPieceDistance;

            return distanceTraveled;
        } 
    }

    public CarPosition Position
    {
        get { return position; }
        private set
        {
            previousPosition = position;
            position = value;
        }
    }

    public double Angle
    {
        get { return angle; }
        set
        {
            previousAngle = angle;
            angle = value;
        }
    }

    public double AngleSpeed
    {
        get
        {
            return Math.Abs(angle) - Math.Abs(previousAngle);
        }
    }

    public double AngleAcceleration
    {
        get { return AngleSpeed - previousAngleSpeed; }
    }

    public bool WillHitMaxAngle()
    {
        const int maxAngle = 54;
        var curve = Race.Curves.SingleOrDefault(c => c.ContainsPiece(Position.piecePosition.pieceIndex));

        // Calculate the max speed for the next curve.  If we can't slow down to the max speed before we reach the next curve, pretend like we'll hit the max angle to slow down.
        double curveLengthLeft = 0;
        if (curve != null)
        {
            var laneIndex = Position.piecePosition.lane.endLaneIndex;
            curveLengthLeft = curve.laneLengths[laneIndex] - curve.laneLengths[laneIndex] * curve.PercentageThroughCurve(laneIndex, CurrentPieceIndex,Position.piecePosition.inPieceDistance);
        }

        // See if we'll ever hit the max angle, if we don't but we're accelerating out of the curve, make sure we don't hit the max angle after the curve.
        bool willHit = Math.Abs(Angle) > maxAngle;
        double calculatedAngle = Math.Abs(Angle);
        int ticksToMaxAngle;
        var distance = 0d;
        var angleAtCurveEnd = 0d;
        for (ticksToMaxAngle = 1; calculatedAngle > 0 && calculatedAngle < maxAngle; ticksToMaxAngle++)
        {
            calculatedAngle += AngleSpeed + AngleAcceleration*ticksToMaxAngle;

            distance += Speed + Acceleration*ticksToMaxAngle;
            curveLengthLeft -= distance;
            if (distance > curveLengthLeft && angleAtCurveEnd == 0d)
            {
                angleAtCurveEnd = calculatedAngle;
                break;
            }
        }

        var maxAngleAfterCurve = angleAtCurveEnd;
        int iteration = 0;
        while (maxAngleAfterCurve > 0 && maxAngleAfterCurve < maxAngle)
        {
            maxAngleAfterCurve += AngleSpeed + AngleAcceleration - .1 * iteration++;
        }
        if (maxAngleAfterCurve > maxAngle || calculatedAngle > maxAngle)
        {
            willHit = true;
        }

        return willHit;
    }

    public double CalculateDistanceToSafeSpeed(double safeSpeed)
    {
        var distance = 0d;
        var tempSpeed = Speed;
        while (tempSpeed > safeSpeed && safeSpeed >= 0)
        {
            tempSpeed = tempSpeed * Friction;
            distance += tempSpeed;
        }

        return distance;
    }

    private int timeangleAccelerationAbove4 = 0;
    private double previousAcceleration;

    public void SetPosition(List<CarPosition> carPositions)
    {
        previousAngleSpeed = AngleSpeed;
        // Must set acceleration before speed as acceleration is calculated from speed.
        previousAcceleration = Acceleration;
        previousSpeed = Speed;

        if (previousPosition == null)
            previousPosition = position;

        Position = carPositions.First(c => c.id.color == id.color);
        Angle = Position.angle;
        CurrentPiece = Race.GetPiece(CurrentPieceIndex);
        // Just trying to make sure the car realizes that it's flinging itself too hard.
        if (Math.Abs(AngleAcceleration) > .4)
        {
            timeangleAccelerationAbove4++;
        }
        else
        {
            timeangleAccelerationAbove4 = 0;
        }

        if (Friction == 0 && Speed > 0 && previousAcceleration > 0)
        {
            Friction = Acceleration/previousAcceleration;
        }

        if (IsInCurve())
        {
            var curve = Race.Curves.SingleOrDefault(c => c.ContainsPiece(CurrentPieceIndex));
            var previousForce = curve.GetCentripetalForceForLaneAtSpeed(previousSpeed, Position.piecePosition.lane.startLaneIndex);
            var percentageThroughCurve = curve.PercentageThroughCurve(Position.piecePosition.lane.endLaneIndex, CurrentPieceIndex, Position.piecePosition.inPieceDistance);
            if (!WillHitMaxAngle())
            {
                Slipping = false;
                if (Race.Curves.Any(c => c.ContainsPiece(previousPosition.piecePosition.pieceIndex)) && previousForce > curve.SlipForce && percentageThroughCurve < .95) {
                    curve.SlipForce = (previousForce + curve.SlipForce) / 2d;
                }
            }
            else
            {
                if (previousForce < curve.SlipForce) {
                    curve.SlipForce = previousForce;
                }
                else if (Slipping) {
                    curve.SlipForce -= .05;
                }
                Slipping = true;
            }
        }

        if (position.piecePosition.lane.startLaneIndex != position.piecePosition.lane.endLaneIndex)
        {
            pendingSwitch = false;
            Status = "";
        }

        SetNextSwitch();
    }

    public bool Slipping { get; set; }

    private void SetNextSwitch()
    {
        // We just started, OR we just passed a switch.
        if (previousPosition == null || previousPosition.piecePosition.pieceIndex != CurrentPieceIndex && Race.GetPiece(previousPosition.piecePosition.pieceIndex).@switch)
        {
            // If we just started, get the first switch.
            if (switchList == null)
                switchList = Race.Switches.Find(Race.Switches.FirstOrDefault(s => s.index.Value >= position.piecePosition.pieceIndex)) ?? Race.Switches.First;
            // We passed a switch, we should increment to the next switch in the list.
            else
                switchList = switchList.Next ?? Race.Switches.First;
        }
    }

    public Switch MakeSwitch()
    {
        pendingSwitch = true;
        Direction directionToSwitch;
        if (distanceInPieces > 0 && position.piecePosition.lane.endLaneIndex == carInFrontOfMe.piecePosition.lane.endLaneIndex)
        {
            var passingDirection = WhereCanISwitch(position);
            if (passingDirection == Direction.Right || passingDirection == Direction.Left) directionToSwitch = passingDirection;
            else directionToSwitch = (Direction) switchList.Value.insideTrack;
        }
        else
        {
            directionToSwitch = (Direction) switchList.Value.insideTrack;
        }

        pendingDirection = directionToSwitch;
        if (directionToSwitch == Direction.Right && position.piecePosition.lane.endLaneIndex == Race.Track.lanes.Count - 1)
        {
            pendingSwitch = false;
        }
        else if (directionToSwitch == Direction.Left && position.piecePosition.lane.endLaneIndex == 0)
        {
            pendingSwitch = false;
        }
        if(pendingSwitch)
            Status = string.Format("SWITCHING TO {0}", directionToSwitch);
        return new Switch(directionToSwitch.ToString());
    }

    public bool ShouldSwitch(List<CarPosition> carPositions, List<CarId> crashedCars)
    {
        if (pendingSwitch || Speed == 0 || TurboOn || switchList.Value.index - 1 != this.Position.piecePosition.pieceIndex)
        {
            return false;
        }

        carInFrontOfMe = CarInFrontOfUs(carPositions, crashedCars);
        var trackPieces = Race.Track.pieces.Count;
        var passingDistanceInPieces = 3;
        if (carPositions.Count == 1) distanceInPieces = -1;
        else if (carInFrontOfMe.piecePosition.pieceIndex == position.piecePosition.pieceIndex)
        {
            if (carInFrontOfMe.piecePosition.inPieceDistance > position.piecePosition.inPieceDistance)
                distanceInPieces = 0;
            else distanceInPieces = -1;
        }
        else
        {
            for (int i = position.piecePosition.pieceIndex; i < (position.piecePosition.pieceIndex + passingDistanceInPieces) % trackPieces; i++)
            {
                if (i == trackPieces) i = 0;
                if (i == carInFrontOfMe.piecePosition.pieceIndex)
                {
                    distanceInPieces = 1;
                    break;
                }
                distanceInPieces = -1;
            }
        }

        if (distanceInPieces == 0 ||
            (distanceInPieces > 0 && position.piecePosition.lane.endLaneIndex != carInFrontOfMe.piecePosition.lane.endLaneIndex))
            return false;
        if (distanceInPieces > 0 && position.piecePosition.lane.endLaneIndex == carInFrontOfMe.piecePosition.lane.endLaneIndex)
            return true;

        if (switchList.Value.insideTrack.ToString() == Direction.Left.ToString() &&
            Position.piecePosition.lane.endLaneIndex != 0 ||
            switchList.Value.insideTrack.ToString() == Direction.Right.ToString() &&
            Position.piecePosition.lane.endLaneIndex != Race.Track.lanes.Count - 1)
        {

            return true;
        }

        return false;
    }

    public bool IsInCurve()
    {
        return CurrentPiece.radius.HasValue;
    }

    public void ProcessCrash()
    {
        Curve curve;
        if (Crashed == false) {
            if (IsInCurve()) {
                curve = Race.Curves.Single(c => c.ContainsPiece(CurrentPieceIndex));
            }
            else {
                curve = Race.Curves.Single(c => c.ContainsPiece(Race.FindPreviousCurve(this)));
            }
            var laneIndex = previousPosition.piecePosition.lane.startLaneIndex;
            var adjustedSpeed = previousSpeed - 1;
            if (curve.MaxSpeedForLane(laneIndex) > previousSpeed)
                if (Math.Abs(curve.MaxSpeedForLane(laneIndex) - adjustedSpeed) < 2)
                    curve.SetMaxSpeedForLane(laneIndex, adjustedSpeed);
                else
                    curve.SetMaxSpeedForLane(laneIndex, previousSpeed);
        }

        Crashed = true;
    }

    public void ProcessSpawn()
    {
        Crashed = false;
    }

    private Direction WhereCanISwitch(CarPosition position)
    {
        int lane = position.piecePosition.lane.endLaneIndex;
        if (lane == 0) return Direction.Right;
        if (lane == Race.Track.lanes.Count - 1) return Direction.Left;
        return Direction.Both;
    }

    private CarPosition CarInFrontOfUs(List<CarPosition> positions, List<CarId> crashedCars)
    {
        List<CarPosition> posits = new List<CarPosition>(positions);
        posits.Sort();
        var index = posits.FindIndex(p => p.id.color == this.id.color);
        var carInFront = posits[(index + 1) % positions.Count];
        while (crashedCars.Any(c => c.color == carInFront.id.color))
        {
            index++;
            carInFront = posits[(index + 1) % positions.Count];
        }
        return carInFront;
    }

    /// <summary>
    /// New race or starting a race after qualifying round. Reset the car so he can find his home.
    /// </summary>
    public void Init()
    {
        switchList = null;
        previousPosition = null;
        position = null;
        pendingSwitch = false;
    }

    public int ExpectedLaneAt(int index)
    {
        if (pendingSwitch)
        {
            return position.piecePosition.lane.endLaneIndex + (pendingDirection == Direction.Right ? 1 : - 1);
        }
        else
        {
            return position.piecePosition.lane.endLaneIndex;
        }
    }

    public void SetTurbo(TurboAvailable turbo)
    {
        TurboAvailable = turbo;
    }

    public TurboAvailable TurboAvailable { get; set; }
    public bool TurboOn { get; set; }
    public double Friction { get; set; }

}